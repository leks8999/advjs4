function getFilms() {
   fetch('https://ajax.test-danit.com/api/swapi/films')
      .then(response => response.json())
      .then(films => displayFilms(films))
}

function displayFilms(films) {
   const filmList = document.createElement('ul')
   films.forEach(film => {
      const filmItem = document.createElement('li');
      filmItem.innerHTML = `<strong>Episode ${film.episodeId}: ${film.name}</strong><br>${film.openingCrawl}`;
      document.body.prepend(filmList)
      filmList.append(filmItem)
      fetchAll(film.characters)
         .then(characters => {
            const filmCharactersList = document.createElement('ul');
            filmCharactersList.innerHTML = '<strong>Characters:</strong>';
            characters.forEach(character => {
               const characterListItem = document.createElement('li');
               characterListItem.textContent = character.name;
               filmCharactersList.append(characterListItem);
            });
            filmItem.append(filmCharactersList);
         })
         .catch(error => console.log(`Error fetching characters for film ${film.episodeId}: ${error}`));
   });
}

function fetchAll(urls) {
   return Promise.all(urls.map(url => fetch(url).then(response => response.json())));
}
getFilms()

